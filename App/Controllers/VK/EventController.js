import VKCallbackAPI from "../../Providers/VKCallbackAPI.js";
import {Event} from "../../Models/VK/Event.js";

export default class EventController {
    /** @param {VKRequest} VKRequest */
    static async handle(VKRequest) {
        switch (VKRequest.type) {
            case Event.CONFIRMATION:
                return '75256ce9';
            case Event.MESSAGE_NEW:
                let message = VKRequest.object.message;
                let peerId = message.peer_id ?? message.user_id;

                VKCallbackAPI.call('messages.send', {
                    random_id: Math.round(Math.random() * 1000),
                    peer_id: peerId,
                    message: 'Я Вас услышала.'
                });

                break;
            default:
                let error = `Event ${VKRequest.type} is not supported`
                console.log(error)
                throw new Error(error);
        }

        return 'ok';
    }
}