import {app} from './config/server.js';
import VKRequestHandler from "./Middleware/VKRequestHandler.js";
import _ from 'body-parser';

app.use(_.json());

app.route('/vk')
    .post((req, res) => {
        new VKRequestHandler(req, res)
    })
    .get((req, res) =>
        res.send('Server is up and running!')
    );