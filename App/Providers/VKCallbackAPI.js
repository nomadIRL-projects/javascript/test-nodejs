import _ from 'request';
import {VK_ACCESS_TOKEN, VK_API_GATEWAY, VK_API_VERSION, VK_GROUP_ID} from "../config/vk.js";

const request = _;

export default class VKCallbackAPI {
    /**
     *
     * @param {String} method
     * @param {Object} params
     * @param {String|null} primary
     */
    static call(method, params = {}, primary = null)
    {
        let queryParams = '';

        Object.entries(params).forEach(([param, value]) => {
            queryParams += `&${param}=${value}`;
        });

        let url = VK_API_GATEWAY
            + method
            + `?access_token=${VK_ACCESS_TOKEN}`
            + `&group_id=${VK_GROUP_ID}`
            + `&v=${VK_API_VERSION}`
            + queryParams;

        request(encodeURI(url),
            (err, response, body) => {
                if (err) {
                    throw new Error(err)
                }

                let answer = JSON.parse(body);
                if (answer.error) {
                    throw new Error(answer.error.error_msg)
                }
            }
        );
    }
}